.. _tezos_community:

The Tezos community
~~~~~~~~~~~~~~~~~~~

- The website of the `Tezos Foundation <https://tezos.foundation/>`_.
- `Tezos Agora <https://www.tezosagora.org>`_ is the main meeting point for the community.
- Several community-built block explorers are available:

    - https://tzstats.com
    - https://tzkt.io (baking focused explorer)
    - https://arronax.io (analytics-oriented explorer)
    - https://baking-bad.org (baking rewards tracker)
    - https://better-call.dev (smart contracts explorer)
    - https://explorus.io/
    - https://explorer.etherlink.com/
    - https://tzflow.com/

- A few community-run websites collect useful Tezos links:

    - https://tezos.com/ecosystem (resources classified by their kind: organisations, block explorers, wallets, etc.)
    - https://tezoscommons.org/ (featured resources classified by approach: technology, developing, contributing, etc.)
    - https://tezos.com/developer-portal/ (resources for developers of applications built on Tezos)

- More resources can be found in the :doc:`support` page.
